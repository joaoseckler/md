title: Exercícios | Curso de CSS/JavaScript/Python
content: programa,curso,css,palavras,javascript,python
lang:pt
nocomments: 1
shortdescription: Exercícios do curso de CSS/js/Python na Palavras

## Exercícios - CSS

### Exercício 1: seletores, combinadores, pseudoclasses e pseudoelementos

Nesse exercício, vamos usar o html do site de um restaurante que está
sem nenhum estilo. Pouco a pouco, vamos deixá-lo apresentável. Para
resolvê-lo, utilize {% lf 'files/e1.epub' 'esse' %} arquivo.

**1.1** Comece estilizando os hyperlinks da página: escolha outra cor,
que não azul para eles, e remova o sublinhado. Que seletor você usou
para fazer essa modificação? Altere somente o CSS para resolver essa
etapa.

**1.2** A primeira imagem é uma imagem de "capa" do site. Ela deve
ocupar mais espaço horizontal que as outras, mas nunca ultrapassando os
limites da página. Para resolver essa etapa, você pode alterar o CSS e o
HTML. Que seletores você usou?

**1.3** Depois de cada imagem há uma legenda. Sem editar o HTML,
estilize essas legendas, alterando o tamanho de fonte e a própria fonte.
Que seletores e combinadores você utilizou?

**1.4** Alguns dos links estilizados no item 1.1 ("About", "Contact" e
"Menu") funcionam como botões: Queremos que a cor de fundo deles se
altere quando passamos com o mouse em cima deles ou quando eles estão
ativos.

**1.5. Iluminuras**

**1.5.1** Use somente CSS para criar iluminuras em todos os
parágrafos (isto é, estilize a primeira letra de cada parágrafo). Para
criar a iluminura, pode ser legal trocar a fonte, colocar uma cor de
fundo, uma borda, estilizar a borda, etc.

**1.5.2** Uma iluminura geralmente não aparece em todos os parágrafos,
mas sim no primeiro de alguma seção. Sem alterar o HTML, modifique o
item anterior para que a iluminura só apareça no primeiro parágrafo de
uma seção.

**1.6** Na lista de bebidas, altere a cor da pimeira, última e quinta
bebida.

**1.7** Suponha quer queiramos avisar aos clientes quais pratos são
veganos e quais não são. É comum colocar a imagem de uma planta ao lado
dos pratos que são veganos. Para isso, encontre uma imagem de planta na
internet adequada, e utilize pseudo-elementos para inserir essa imagem
após os itens veganos. Você deve usar HTML para indicar quais pratos são
veganos e quais não são, mas você não deve inserir a imagem direto no
HTML!


### Exercício 2: Cascata, especificidade e herança

*Inspirado [nesse artigo](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance#conflicting_rules).*

**2.1** Considere o seguinte código:

```html
<ul class="main">
    <li>Item One</li>
    <li>Item Two
        <ul>
            <li>2.1</li>
            <li>2.2</li>
        </ul>
    </li>
    <li>Item Three
        <ul class="special">
            <li>3.1
                <ul>
                    <li>3.1.1</li>
                    <li>3.1.2</li>
                </ul>
            </li>
            <li>3.2</li>
        </ul>
    </li>
</ul>
```

```css
.main {
    color: orange;
    border: 2px solid #ccc;
    padding: 1em;
}

.special {
    color: black;
    font-weight: bold;
    border: 1px solid #fc9;
}
```

**2.1.1** Que parte do html terá fonte com cor laranja, e que parte terá
fonte de cor preta?

**2.1.2** Das propriedades atribuídas no trecho CSS quais são herdadas e
quais não são?

**2.1.3** Use o valor `inherit` e o combinador de descendente imediato
para reproduzir a imagem abaixo:

![alguns retângulos produzidos por HTML e CSS]({% lstatic 'files/2.1.3.png' %})


**2.2** Considere o seguinte código:

```html
<ul>
    <li>Default <a href="#">link</a> color</li>
    <li class="class-1">Inherit the <a href="#">link</a> color</li>
    <li class="class-2">Reset the <a href="#">link</a> color</li>
    <li class="class-3">Unset the <a href="#">link</a> color</li>
</ul>
```

```css
body {
    color: green;
}
```

**2.2.1** Complete o CSS de modo que o link de `class-1` tenha `color:
inherit`, o link de `class-2` tenha `color: initial` e o link de
`class-3` tenha `color: unset`. O que vai acontecer em cada um dos
casos? Procure responder antes de implementar.

**2.2.1** O que a regra `.my-class-1 a` está fazendo? Ela tem efeito,
visto que a cor já é uma propriedade herdada?

**2.2.2** Sabemos que se usarmos um link sem nenhum estilo, ele aparece
azul. Porque ao usarmos o valor `initial`, o link fica preto, e não
azul? Que valor teríamos que usar para deixar o link da mesma cor que
ele ficaria se não fosse estilizado?

**2.3** Considere os seguintes códigos:

```css
/* 1. */
#outer a {
    background-color: red;
}

/* 2. */
#outer #inner a {
    background-color: blue;
}

/* 3. */
#outer div ul li a {
    color: yellow;
}

/* 4. */
#outer div ul .nav a {
    color: white;
}

/* 5. */
div div li:nth-child(2) a:hover {
    border: 10px solid black;
}

/* 6. */
div li:nth-child(2) a:hover {
    border: 10px dashed black;
}

/* 7. */
div div .nav:nth-child(2) a:hover {
    border: 10px double black;
}

a {
    display: inline-block;
    line-height: 40px;
    font-size: 20px;
    text-decoration: none;
    text-align: center;
    width: 200px;
    margin-bottom: 10px;
}

ul {
    padding: 0;
}

li {
    list-style-type: none;
}
```

**2.3.1** Dê a nota de especificidade para cada uma das 7 regras
numeradas.

**2.3.2** Que regras estão competindo para definir a cor de fundo para
links (`<a>`)? Explique qual está "ganhando" e por quê.

**2.3.3** Repita o exercício anterior, mas agora para a cor do texto.

**2.3.4** Repita o exercício anterior, mas agora para a cor do texto.

**2.3.5** Quais regras estão competindo para definir o estilo da borda
para os links (`<a>`)? Qual está ganhando para o primeiro link, e qual
está ganhando para o segundo? Porque?

**2.3.6** Escreva uma regra que, se usada na sequência do código CSS
apresentado, vai estilizar a borda do primeiro link, mas não do segundo.

### Exercício 3: Box model

*Inspirado
[nesses](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Box_Model_Tasks) exercícios.*

**3.1** Considere os seguintes códigos:

```
<span class="box">Eu sou uma caixa!</span>
```

```css
.box {

}
```

**3.1.1** Faça as seguintes modificações na caixa:

* Defina a largura da caixa como 100px e seu comprimento como 150px;
* Adicione uma borda de 5px, preta e pontilhada;
* Uma margem superior ("top margin") de `20px`;
* Uma margem direita de `1em`;
* Uma margem inferior de `40px`;
* Uma margem esquerda de `2em`;
* Preenchimento ("padding") vertical de `1em` e horizontal de `0.5em`.

**3.1.2** Refaça o exercício anterior usando a notação abreviada para
margem, preenchimento e borda (se é que você já não usou!).

**3.1.3** Considerando que o tamanho de fonte é de `16px`, e portanto
`1em` = `16px`, calcule a altura e a largura total da caixa.

**3.1.4** Utilize a valor `border-box` para a propriedade `box-sizing`.
Em seguida, refaça o exercício anterior.


**3.2** Considere os seguintes códigos:

```html
<div class="box">
  <p>Veggies es bonus vobis, <span>proinde vos postulo</span> essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.</p>

  <p>Gumbo beet greens corn soko endive gumbo gourd. Parsley shallot courgette tatsoi pea sprouts fava bean collard greens dandelion okra wakame tomato. Dandelion cucumber earthnut pea peanut soko zucchini.</p>
</div>
```

```css
.box span {
  background-color: pink;
  border: 5px solid black;
  padding: 1em;
}
```

**3.2.1** Ao executá-los, vê-se que o preenchimento e a borda do `span`
estão interferindo com outras linhas do parágrafo. Como fazer o elemento
manter essas propriedades, mas ainda permanecendo no meio da linha?

### Exercício 4: Variáveis em cascata

**4.1** Use propriedades customizáveis para criar um parágrafo que,
quando passamos o mouse em qualquer lugar dele, sua última palavra fica
pintada de roxo.

**4.2** Considere os seguintes códigos:

```html
<body>
  <p>Uma citação famosa:</p>
  <blockquote class="container">
    <cite>"Quando Gregor Samsa, certa manhã, despertou de sonhos
    intranquilos, encontrou-se em sua cama metamorfoseado em um inseto
    monstruoso."
    </cite>
    <span>A metamorfose</span>
  </blockquote>
</body>
```

```css
:root {
    --color: red;
}

blockquote {
    --color: gray;
}

blockquote span {
    color: var(--color);
}

cite {
    --color: orange;
    color: var(--color);
}

p {
    color: var(--color);
}
```

Qual será a cor do texto no parágrafo? E no `cite`? E no `span`?

### Exercício 5: CSS grid

Reproduza a imagem abaixo usando CSS grid:

![alguns retângulos desenhados]({% lstatic 'files/5.png' %})


## Exercícios - Javascript

### Exercício 1: o console do navegador

**1.1** Escreva expressões em javascript correspondentes aos seguintes
comandos:

* A soma dos 5 primeiros inteiros positivos.
* A idade média de Sara (idade 23), Mark (idade 19) e Fátima (idade 31).
* O número de vezes que 73 cabe em 403.
* O resto de quando 403 é dividido por 73.
* 2 à 10a potência.
* O valor absoluto da distância entre a altura de Sara (54 polegadas) e a altura de Mark (57 polegadas).
* O menor preço entre os seguintes preços: R$ 34,99, R$ 29,95 e R$ 31,50

**1.2** Escreva expressões em javascript correspondentes às seguintes
afirmações booleanas

* A soma de 2 e 2 é menor que 4.
* O valor de 7 / 3 é igual a 1 + 1.
* A soma de 3 ao quadrado e 4 ao quadrado é igual a 25.
* A soma de 2, 4 e 6 é maior que 12.
* 1387 é divisível por 19.
* 31 é par. (Dica: o que o resto lhe diz quando você divide por 2?)
* O preço mais baixo dentre R$ 34,99, R$ 29,95 e R$ 31,50 é menor que R$ 30,00.

**1.3** Escreva expressões que correspondem às ações a seguir e execute-as:

*  Atribua o número 3 à variável a.
*  Atribua 4 à variável b.
*  Atribua à variável c o valor da expressão a * a + b * b.

**1.4** Escreva expressões em javascript correspondentes ao seguinte:

* O comprimento da hipotenusa em um triângulo retângulo cujos dois
  outros lados têm comprimentos `a` e `b`
* O valor da expressão que avalia se o comprimento da hipotenusa acima é
  5
* A área de um disco com raio `a`
* O valor da expressão Booleana que verifica se um ponto com coordenadas
  `x` e `y` está dentro de um círculo com centro `(a, b)` e raio `r`

**1.5**

```js
let palavras = ['taco', 'bola', 'celeiro', 'cesta', 'peteca']
```

Agora, escreva duas expressões que são avaliadas, respectivamente, como
a primeira e a última palavras em palavras, na ordem do dicionário.

**1.6**

Em que ordem os operadores nas expressões a seguir são avaliados?

* `2 + 3 == 4 || a >= 5`
* `lst[1] * -3 < -10 == 0`
* `2 * 3**2`


**1.7**

Qual é o tipo do valor ao qual essas expressões são avaliadas?

* `False + False`
* `2 * 3**2.0`
* `4 // 2 + 4 % 2`
* `2 + 3 == 4 or 5 >= 5`


**1.8**

Comece executando

```js
let s = 'goodbye'
```

Depois, escreva uma expressão Booleana que verifica se:

* O primeiro caractere da string s é `g`
* O sétimo caractere de s é `g`
* Os dois primeiros caracteres de s são `g` e `a`
* O penúltimo caractere de s é `x`
* O caractere do meio de s é `d`
* O primeiro e último caracteres da string s são iguais

Nota: Essas sete instruções devem ser avaliadas como `true`, `false`,
`false`, `false`, `true`, `false` e `false`, respectivamente.

**1.9**

Um alvo de dardos de raio 10 e a parede em que está pendurado são
representados usando o sistema de coordenadas bidimensionais, com o
centro do alvo na coordenada `(0, 0)`. As variáveis `x` e `y` armazenam
as coordenadas `x` e `y` de um lançamento de dardo. Escreva uma
expressão usando as variáveis `x` e `y` que avalia como True se o dardo
atingir o (estiver dentro do) alvo, e avalie a expressão para estas
coordenadas do dardo:

a. `(0, 0)`

b. `(10, 10)`

c. `(6, –6)`

d. `(–7, 8)`


**1.10** FizzBuzz

**1.10.1** Dada uma variável `n`, imprima `par` quando ela for par
ou `ímpar` ela for ímpar.

**1.10.2** Dada uma variável `n`, imprima `Fizz` se ela for divisível
por 3. Caso contrário, imprima a própria variável.

**1.10.3** Dada uma variável `n`, imprima `Buzz` se ela for divisível
por 5. Caso contrário, imprima a própria variável

**1.10.4** Dada uma variável `n`, imprima `FizzBuzz` na saída se ela for
divisível por 3 e por 5. Caso contrário, imprima a própria variável.

**1.10.5** Dada uma variável `n`, imprima:

* `Fizz` se ela for divisível por 3;
* `Buzz` se ela for divisível por 5;
* `FizzBuzz` se ela for divisível por 3 e por 5;
* a própria variável, caso contrário.

### Exercício 2: Objetos

**2.1** Considere o seguinte objeto:

```js
const cao1 = {
  nome: "Bidu",
  cor: "azul",
  raca: "schnauzer",
};
```

**2.1.1** Defina uma variável `cao2` que contenha informação de nome,
cor e raça de algum outro cachorro.

**2.1.2** Imprima na tela a seguinte mensagem. Seu código deve
utilizar os valores da variável `cao1`, de modo a ser facilmente
adaptado para a variável `cao2`.

```txt
Esse é o Bidu. Ele é azul e é da raça schnauzer.
```


**2.2** Considere o seguinte objeto:

```js
const casa = {
  rua: "Alameda Nothmann",
  numero: "88",
  bairro: "Campos Elíseos",
  cidade: "São Paulo",
  UF: "SP",
  CEP: "01216-001",
};
```

**2.2.1** Repare que há um problema de definição no objeto: A
propriedade diz que é uma rua, mas o valor diz que é uma Alameda!
Defina esse objecto novamente, separando a informação acima em dois
campos: `tipo_logradouro`, que diz se trata-se de uma rua, avenida,
alameda, etc; e `logradouro`, que informa somente o nome do logradouro.

**2.2.2** Dado o obeto `casa` resultado do exercício anterior, o que o
código abaixo vai imprimir?

```js
console.log(casa.bairro);
console.log(casa["cidade"]);

let prop = "logradouro";
console.log(casa[prop]);

prop = "tipo_" + prop;
console.log(casa[prop]);
```

**2.3** Qual o valor final que estará armazenado em `x` e em `y`?

```js
let x = 50;
let y = 20;
let aux = x;
x = y
y = aux;
```

*Para implementar as funções dos próximos exercícios, faça o download
{% lf 'files/funcoes.zip' 'dessa' %} pasta e implemente-as no arquivo
`script.js`.*

**2.4** Implemente as seguintes funções:

**2.4.1** Uma função que imprime a temperatura em Farenheits equivalente
a 34 graus Celsius.

**2.4.2** Uma função que recebe uma temperatura (tipo `Number`) em graus
Celsius e *imprime* a temperatura equivalente em Farenheit.

**2.4.2** Uma função que recebe uma temperatura (tipo `Number`) em graus
Celsius e *devolve* a mesma temperatura em graus Farenheit.

**2.5** Implemente a função `bissexto`, que recebe um ano (tipo
`Number`) e imprime `Pode ser um ano bissexto` se o ano for divisível
por quatro; caso contrário, imprime `Definitivamente não é um ano
bissexto`.

**2.6** Implemente a função `iniciais`, que recebe duas string: o nome e
o sobrenome de uma pessoa. Essa função *devolve* uma string
correspondente às iniciais da pessoa. As iniciais de Albert Einstein,
por exemplo, são "AE".

**2.6** Implemente as seguintes funções:

**2.6.1** Uma função `area`, que recebe a medida do lado de um quadrado
e devolve sua área.

**2.6.2** Uma função `perimetro`, que recebe a medida do lado de um
quadrado e devolve seu perímetro.

**2.6.3** Uma função `infos_quadrado`, que recebe a medida do
lado de um quadrado e devolve as informações de área e perímetro dele.
As informações devem estar no seguinte formato:

```txt
Área: <área> - Perímetro: <perímetro>
```

Essa função deve se valer das funções implementadas nos dois exercícios
anteriores!

**2.6.4** Uma função `area_retangulo` que recebe como argumento a altura
e a largura de um retângulo e devolve sua área

**2.6.5** Uma função `perimetro_retangulo` que recebe como argumentos a
altura e a largura de um retângulo e devolve seu perímetro

**2.6.6** Uma função `infos_retangulo`, que recebe a como argumentos a
largura e altura de um regângulo e devolve informações de área e
perímetro dele. Deve estar no mesmo formato que o exercício 2.6.3

**2.6.7** Uma função `area_triangulo_retangulo`, que recebe como
argumentos a medida dos três lados de um triângulo retângulo e calcula
sua área.

**2.7** O *quebra-sol* é aquela parte do carro que é uma aba disposta
acima do motorista e do passageiro. É bastante comum encontrar no
ao redor do quebra-sol um espelho e uma luz.

![Quebra-sol vermelho com luz e espelho]({% lstatic 'files/quebra-sol.jpg' %} "Quebra-sol")

Nesse exercício, vamos escrever uma função `luz_ligada` que descreve as
situações em que a luz está acesa. Isso depende de alguns fatores, que
serão os argumentos da sua função:

* `carro_ligado`, um valor Booleano que indica se o carro está ligado;
* `aberto`, um valor Booleano que indica se o quebra-sol está aberto;
* `espalho_visivel`, um valor Booleano que indica se o espelho do
  quebra-sol está visível.

A partir desses argumentos, sua função deve devolver um valor Booleano
que indica se a luz do quebra-sol está ligada.
