title: Materiais | Curso de CSS/JavaScript/Python
content: programa,curso,css,palavras,javascript,python
lang:pt
nocomments: 1
shortdescription: Materiais do curso de CSS/js/Python na Palavras


## Materiais usados em aula

### Cascata, herança e especificidade

{% lf 'files/cascata.epub' 'epub' %}

{% lf 'files/cascata.pdf' 'pdf' %}


### Seletores, combinadores, pseudoelementos e pseudoclasses

{% lf 'files/seletores-combinadores.epub' 'epub' %}

### Box model

{% lf 'files/box-model.pdf' 'pdf' %}

{% lf 'files/box-model.epub' 'epub' %}

### Grid

{% lf 'files/grid.pdf' 'pdf' %}

{% lf 'files/grid.epub' 'epub' %}

{% el 'https://css-tricks.com/snippets/css/complete-guide-grid/' 'referência em inglês' %}

{% el 'https://www.origamid.com/projetos/css-grid-layout-guia-completo/' 'referência em português' %}

### Animações

video:

{% video 'files/screencast' 'webm' 'mkv' name='Screencast sobre animações' %}

{% lf 'files/animation.epub' 'epub' %}
