title: Curso de CSS/JavaScript/Python
content: programa,curso,css,palavras,javascript,python
lang:pt
nocomments: 1
shortdescription: Programa do curso para a editora Palavras

## Curso de CSS/Javascript/Python
**Palavras Projetos Editoriais**

* Aulas às terças-feiras, às 17h, online;
* Início das aulas: 20/06/2023;
* Email do professor: joaoseckler [arroba] gmail.com. Qualquer dúvida,
sugestão, problema, por favor, me escreva!

### Apresentação

Esse curso tem objetivo duplo: primeiro, vamos nos aprofundar em
ferramentas para design de livros digitais e para desenvolvimento web —
CSS e Javascript —, firmando seus conceitos fundamentais e
experimentando algumas funcionalidades avançadas, focando principalmente
naquilo que faz sentido para a criação de EPUBs. Depois, vamos travar
contato com o pensamento computacional e a lógica de programação, usando
como ferramentas duas importantes linguagens de programação: JavaScript
e Python.

A ideia é ganharmos autonomia para resolver problemas que antes não
conseguiríamos sozinhos e, quem sabe, resolver com mais qualidade os
problemas que já sabemos enfrentar.

As aulas terão momentos de conversa, em que alguns conceitos chave serão
apresentados e debatidos. A principal atividade, contudo, será a
resolução de exercícios. Alguns vão ser resolvidos em grupo, outros
individualmente. Alguns serão mais simples, e servem mais para fixar
algum tema, outros vão se parecer mais com situações reais (quem sabe,
até serão problemas reais...) e exigirão mais criatividade e
engenhosidade.

### Conteúdos

Os conteúdos apresentados são uma sugestão de fio condutor para o
curso. Cada tópico referencia uma página em que mais informações sobre
ele podem ser encontradas.

#### Tópicos em CSS

Duração estimada: 4 aulas.

1.  [Seletores](https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors)
    e [combinadores](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors/Combinators)
2.  [Pseudoclasses e pseudoelementos](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors/Pseudo-classes_and_pseudo-elements)
3.  Porque "cascading"? [Cascata, especificidade e herança](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)
4.  O [modelo de caixa](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model)
6.  [Variáveis](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)
    e [cálculo](https://developer.mozilla.org/en-US/docs/Web/CSS/calc) no CSS
7.  [CSS layout](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout):
    Grid vs. Flexbox
8.  [Adaptando-se à tela do usuário: design responsivo](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Responsive_Design)
9.  [Transformações](https://developer.mozilla.org/en-US/docs/Web/CSS/transform),
    [transições](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)
    e [animações](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_animations/Using_CSS_animations)
9.  Sistema hexadecimal e [sistemas de cores](https://www.smashingmagazine.com/2021/11/guide-modern-css-colors/)
    (RGB, HSL e LCH)
10. [Acessibilidade](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/CSS_and_JavaScript)
    em páginas HTML/CSS
11. [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Getting_Started)
    e [animações](https://css-tricks.com/animating-svg-css/)

#### Javascript

Duração estimada: 8 aulas

1.  O que é [JavaScript](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics)?
2.  Javascript no [console](https://developer.mozilla.org/en-US/docs/Web/API/console):
3.  [Expressões e valores](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_operators)
4.  [Tipos, operações](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures) e coerção de tipos
7.  [Mensagens de erro](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_went_wrong)
8.  Javascript [interno (inline) e externo](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript#how_do_you_add_javascript_to_your_page)
9.  [`console.log`](https://developer.mozilla.org/en-US/docs/Web/API/console)
10. [DOM:](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript#so_what_can_it_really_do) `querySelector` e `getElementById`.
11. [Condicionais](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics#conditionals)
12. [Funções](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics#functions)
13. [Iteração](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Looping_code): `while` e `for`
14. [Event listeners](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)
15. [Objetos](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects)

#### Python

Duração estimada: 8 aulas

1.  [Setup](https://arco.coop.br/~jseckler/comp-2023/setup.html) do ambiente
2.  O que é Python?
3.  O [console](https://arco.coop.br/~jseckler/comp-2023/topicos.html#express%C3%B5es-e-instru%C3%A7%C3%B5es) do Python
3.  [Expressões e instruções](https://arco.coop.br/~jseckler/comp-2023/topicos.html#express%C3%B5es-e-instru%C3%A7%C3%B5es)
3.  Mensagens de [erro](https://arco.coop.br/~jseckler/comp-2023/topicos.html#erro)
4.  [Tipos, operações e conversão](https://arco.coop.br/~jseckler/comp-2023/topicos.html#tipos): diferenças para o js
4.  [Linha de comando vs. scripts](https://arco.coop.br/~jseckler/comp-2023/topicos.html#linha-de-comando-vs.-scripts-ou-programas)
5.  [Condicionais](https://arco.coop.br/~jseckler/comp-2023/topicos.html#if-else-e-elif)
6.  [Iteração](https://arco.coop.br/~jseckler/comp-2023/topicos.html#laços): `while`, `for`, `range`
7.  [Funções](https://arco.coop.br/~jseckler/comp-2023/topicos.html#funções)
8.  [Laços aninhados](https://www.geeksforgeeks.org/python-nested-loops/)
9.  [Expressões regulares](https://docs.python.org/3/library/re.html) (regex)
10. [Plugins para o Sigil](http://www.mobileread.mobi/forums/showthread.php?t=263081)
