keywords: joão,seckler,vim,audo,transcription,fifo,mplayer,slave,vimrc,leaderkey
shortdescription: Transcribe efficiently with vim and mplayer
footerlink: stuff
lang: en

## Audio transcription with vim

We'll show a way to control an audio from within vim. This makes life
easier when you are doing audio transcription; you no longer have to
press Alt-tab every time you want to pause, fast-forward or go back in
the file.


We'll be using `mplayer` due to its ability to run in slave mode, where
it "works as a backend for other programs" (`man mplayer`). You
specify an input file (stdin, for example) from which `mplayer` will
listen in order to be commanded.


So, we set up a fifo (first in first out, a sort of socket), tell
`mplayer` to listen to it and set up mappings in vim to talk to it.
Done!

<br>
Create the fifo:

`$ mkfifo /tmp/fifo`

Call mplayer in slave mode:

`mplayer -slave -input file=/tmp/fifo file.mp3`

Some mappings suggestions in `.vimrc`:

```vim
function Transc()
  """"""""""""
  " For audio transcription
  " Start with
  "     mkfifo /tmp/fifo;
  "     mplayer -slave -input file=/tmp/fifo <playback file>
  """""""""""""

  inoremap <C-p> <Esc>:silent !echo pause > /tmp/fifo<CR><C-l>a
  inoremap <C-o> <Esc>:silent !echo seek 1 > /tmp/fifo<CR><C-l>a
  inoremap <C-i> <Esc>:silent !echo seek -1 > /tmp/fifo<CR><C-l>a
  inoremap <C-k> <Esc>:silent !echo seek 5 > /tmp/fifo<CR><C-l>a
  inoremap <C-j> <Esc>:silent !echo seek -5 > /tmp/fifo<CR><C-l>a

  nnoremap <Leader>p <Esc>:silent !echo pause > /tmp/fifo<CR><C-l>
  nnoremap <Leader>o <Esc>:silent !echo seek 1 > /tmp/fifo<CR><C-l>
  nnoremap <Leader>i <Esc>:silent !echo seek -1 > /tmp/fifo<CR><C-l>
  nnoremap <Leader>k <Esc>:silent !echo seek 5 > /tmp/fifo<CR><C-l>
  nnoremap <Leader>j <Esc>:silent !echo seek -5 > /tmp/fifo<CR><C-l>
  nnoremap <Leader>] <Esc>:silent !echo speed_incr 0.1 > /tmp/fifo<CR><C-l>
  nnoremap <Leader>[ <Esc>:silent !echo speed_incr -0.1 > /tmp/fifo<CR><C-l>
endfunction
```

Don't forget to `:call Transc()` to use it. You can find the list of
accepted commands {% el 'http://www.mplayerhq.hu/DOCS/tech/slave.txt' 'here' %}.
