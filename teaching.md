keywords: joão,seckler,teaching,css,javascript,python,arco
shortdescription: Teaching materials
lang: en
en_version: ensino
footerlink: index_en

## Ensino

**{% el 'https://arco.coop.br/~jseckler' 'Personal page at Arco' %}**

Materials from maths and computer science courses taught at Arco Escola
Cooperativa.

**{% il 'ensino/palavras' 'Course at Palavras' %}**

Materials from a CSS/Javascript course taught at
{% el 'https://palavraseducacao.com.br' 'Palavras Projetos Editoriais' %}.
