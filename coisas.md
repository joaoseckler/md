keywords: joão,seckler,música,john,cage,elomar,funcao,campo,branco,imaginary,landscape,supercollider,onion,vim,eotk,trancrição
shortdescription: Coisas relacionadas principalmente a musica e computadores
hascomments: 1
lang: pt
en_version: stuff

## Coisas

**{% el 'elami' 'Elami' False %}** Elami é um aplicativo de treinamento de
percepção musical que oferece exercícios cuja interação é
inteiramente por áudio. Os exercícios contam com algum tipo de
desafio, que, após uma pausa, se segue de algum tipo de solução.
Tudo só pra conseguir estudar percepção dentro do ônibus lotado! O
software, escrito em react, ainda está em fase de prototipação. Seu
código fonte encontra-se
{% el 'https://gitlab.com/joaoseckler/elami' 'aqui' %}.

**{% el 'cifras' 'Cifras' False %}** Algumas cifras, principalmente de
sambas. Conta com transposição automática, quebra de linhas
consistentes e auto rolamento da página. As páginas web foram
geradas automaticamente a partir de arquivos de textos simples. O
código que faz isso e os arquivos fonte estão disponíveis
{% el 'https://gitlab.com/joaoseckler/cifras' 'aqui' %}.

**{% el 'repetition' 'Répétition' %}** Software para prática de
repertório musical com
{% el 'https://pt.wikipedia.org/wiki/Repeti%C3%A7%C3%A3o_espa%C3%A7ada' 'repetição espaçada' %}:
como "cards {% el 'https://anki.com.br' 'Anki' %}", mas voltada para músicos. Cada
música corresponde a um "card", em que se pode colocar um vídeo do
youtube, uma partitura em PDF, um arquivo de áudio etc. Cada música faz
parte de um repertório e, para cada repertório, o software sugere qual
música deveria ser ensinada em seguida, baseado no tempo desde o último
ensaio e na dificuldade apontada pelo usuário quando do último ensaio.
O software usa o
{% el 'https://github.com/open-spaced-repetition/free-spaced-repetition-scheduler' 'algoritmo FSRS' %}
para fazer esse cálculo. Usuários também podem compartilhar músicas e
repertórios. Talvez isso fosse mais facilmente implementado como um
conjunto de "cards Anki", mas foi uma boa desculpa para aprender uma
nova ferramenta, {% el 'https://nextjs.org/' 'Next.js' %}. Répétition em françês
quer dizer ao mesmo tempo repetição e ensaio, que é para que serve esse
programa. Código fonte dos fundos está
{% el 'https://gitlab.com/joaoseckler/repetition' 'aqui' %} e da fachada
está {% el 'https://gitlab.com/joaoseckler/website' 'aqui' %}.

**{% el 'https://biblioteca.arco.coop.br' 'Biblioteca' True %}** Sistema
de gerenciamento de bibliotecas bastante simples. Desde meados de 2024
funciona como sistema da biblioteca da
{% el 'https://arco.coop.br' 'Arco Escola Cooperativa' %}. Inclui classificação
e catalogação de livros, gerenciamento de usuários e moderadores,
gerenciamento e notificações de empréstimos, impressão de etiquetas,
backups etc. A biblioteca da Arco tem uma parte pública acessível no
link acima, mas é possível xeretar todas as funcionalidades numa
instância em modo "sandbox" disponível {% el 'biblioteca' 'aqui' %}.
Nela o usuário pode fazer qualquer modificação, mas o banco
de dados é revertido todos os dias ao seu estado inicial.
O sistema não inclui importação de autoridades nem sabe lidar com
arquivos Marc. Alternativas a ele incluem o
{% el 'https://biblivre.org.br/' 'Biblivre' %} e o
{% el 'https://folio.org/' 'Folio' %}.
Aqui está o {% el 'https://gitlab.com/joaoseckler/biblioteca' 'código fonte' %}.

**{% il 'partituras' "Partituras" %}** Partituras diversas

**{% il 'livreto' 'Conversão para livreto' %}** Você dá um PDF e a gente
devolve ele em formato de livreto.

**{% il 'lyrics2ly' 'Escansão para LilyPond' %}**
Nessa página você pode separar letras de músicas em sílabas, de acordo
com a sintaxe de um bloco `\lyricmode` do
[LilyPond](https://lilypond.org). Também tem algumas dicas pra quem mexe
com vim e lilypond ao mesmo tempo.

**{% il 'palco' 'Guia de movimentação de palco para coralistas' %}**
Algumas ideias e animações sobre movimentação de palco de corais.

**{% il 'html2gc' 'Conversão de texto formatado para GreenCloth' %}**
Converta texto formatado (p.ex., copiado de um site ou de um processador
de texto) para GreenCloth, uma versão de Markdown usada na
{% el 'https://we.riseup.net' 'Riseup We' %}.

**{% el 'https://github.com/joaoseckler/imaginary-landscape-5' 'Imaginary Landscape No.5' %}**
Script em Bash que chama um
script em [SuperCollider](https://supercollider.github.io/). A
partir de arquivos de áudio fornecidos pelo usuário, ele reproduz ou
grava uma versão da música do John Cage "Imaginary Landscape número
5". Sobre a música, ver também {% sl 'imaginary-landscape.pdf' 'isso' %},
{% el 'https://en.wikipedia.org/wiki/Imaginary_Landscape' 'isso' %}
e {% el 'https://www.youtube.com/watch?v=bu38ZidvmEY' 'isso' %}.

**{% il 'vim-transcription' 'Transcrição de áudio com vim' %}**
Configuração do `.vimrc` para auxiliar transcrição de áudio. Controla
o mplayer de dentro do vim. Para isso, criamos um fifo ou pipe e
chamamos o mplayer no modo slave. Depois, mapeamos alguns atalhos no
vim para enviar comandos para esse fifo.

**{% il 'onion-service' 'Onion service e uns hacks no eotk' %}**
Algumas dicas pra usar o `eotk`, ou seja, pra criar um onion service
que espelha algum site que já está no ar. As dicas ajudam a criar
endereços v3 e direcionar todo o tráfico tor para http simples.

**{% il 'torrents' 'Torrents' %}** Alguns torrents

**{% lf 'musica/exercicios-harmonia.pdf' 'Trinta exercícios de harmonia "popular"' %}**
Material que organizei como forma de estudo em 2018, baseados em
aulas no Souza Lima em 2012 e na EMESP em 2017.
