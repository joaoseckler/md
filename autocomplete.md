## Bug with glob autocompletion in Bash

Consider we are in the following folder:

```txt
.
├── 1.txt
├── 2.txt
├── 3.txt
├── 4.txt
└── 5.txt
```

It is really nice when the autocompletion of the following command

```bash
$ ls *.txt
```

is

```bash
$ ls *.txt<Tab><Tab>
1.txt  2.txt  3.txt  4.txt  5.txt
```

In some debian based distributions there is a bug preventing
autocompletion to occur completely in such cases. Fixing it is as simple
as applying the following patch to the `_filedir` function in
`/usr/share/bash-completion/bash_completion`:

```diff
--- a/old
+++ b/new
@@ -617,6 +617,8 @@ _filedir()
         # 2>/dev/null for direct invocation, e.g. in the _filedir unit test
         compopt -o filenames 2>/dev/null
         COMPREPLY+=("${toks[@]}")
+    else
+        compopt -o bashdefault 2>/dev/null
     fi
 } # _filedir()
```

<hr>

This solution comes from a comment to
[this](https://bugs.launchpad.net/ubuntu/+source/bash-completion/+bug/1387057/)
historical bug report. There is
[another](https://bugs.launchpad.net/ubuntu/+source/bash-completion/+bug/1361404/)
very old bug report, but the comments suggested there had no effect as
far as my testing went.

