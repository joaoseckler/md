title: João Seckler
keywords: joão,seckler
shortdescription: Algumas coisas para compartilhar
footerlink: index


## How to use this md engine

The first couple of line carry metadata information:

```text
hascomments: 1
nocomments: 0
nofooter: 0
js_head: url https://cdnjs.cloudflare.com/ajax/libs/p5.js/1.7.0/p5.min.js
         integrity="sha512-bcfltY+lNLlNxz38yBBm/HLaUB1gTV6I0e+fahbF9pS6roIdzUytozWdnFV8ZnM6cSAG5EbmO0ag0a/fLZSG4Q=="
         crossorigin="anonymous"
         referrerpolicy="no-referrer"
css: static myapp/style.css
js_body: myapp/js/script.js
         defer
         static myapp/js/script2.js
```

Please note:

- The only way to set an attribute to false is deleting it
- Title is deduced from html and footerlink from folder structure
- shortdescription and keywords should be set in md file

You can use django templating language inside the md
Particularly, use the templatetags il, el, sl and lf (for internal,
external, static links and local files, respecitvely) like this:
`{% verbatim %}{% il 'url' 'label' %}{% endverbatim %}`

The tag `{% verbatim %}{% utf %}{% endverbatim %}` is also available,
which simply gives you a random utf value.

## Meta-data

from [the documentation](https://python-markdown.github.io/extensions/meta_data/):

The keywords are case-insensitive and may consist of
letters, numbers, underscores and dashes and must end with a colon.
The values consist of anything following the colon on the line and may
even be blank.

If a line is indented by 4 or more spaces, that line is assumed to be an
additional line of the value for the previous keyword. A keyword may
have as many lines as desired.

The first blank line ends all meta-data for the document. Therefore, the
first line of a document must not be blank.

Alternatively, You may use YAML style deliminators to mark the start
and/or end of your meta-data. When doing so, the first line of your
document must be ---. The meta-data ends at the first blank line or the
first line containing an end deliminator (either --- or ...), whichever
comes first. Even though YAML deliminators are supported, meta-data is
not parsed as YAML.

All meta-data is stripped from the document prior to any further
processing by Markdown.
