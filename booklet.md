keywords: joão,seckler,booklet,pdfjam,pdf,convert,signature
shortdescription: Convert PDF to booklet format
hascomments: 1
lang: en
footerlink: stuff

{% load booklet %}

## Conversion to booklet form

In this page, you provide a PDF file and we return it in booklet form.

{% booklet_form %}

### What is a booklet?

A PDF generated here will have two pages of the original file in each of
its own pages. It is organized in such a way that, after printing, it is
enough to follow these steps to achieve the correct page arrangement:

1. Divide the document into blocks, each containing _n_ sheets;
2. Fold each block in half;
3. Bind all the folded blocks together, maintaining the original
   printing order.

Here, the value of _n_ depends on the chosen signature size. The
signature size determines the number of pages from the original file
that are present in each block. Since each physical sheet includes four
of these pages, _n_ is equal to the number of signatures divided by
four.


### Why this?

Booklet printing is pretty common by default in several printing
dialogue softwares, but usually they are only able to print one
signature. This page is useful if you have a big PDF which you want to
turn into several signatures and print all at once.
