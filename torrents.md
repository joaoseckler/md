keywords: joão,seckler,torrent,marcus,pereira,hegel
shortdescription: Algumas coisas para compartilhar
lang: pt
footerlink: coisas
en_version: torrents_en

## Torrents

**{% el 'magnet:?xt=urn:btih:8a95fb298579e98f122e5aee1a86e7e46e8013d2&dn=The+Official+UK+Top+40+Singles+Chart+%2808.12.2017%29+Mp3+%28320kbps%29&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969' 'Marcus Pereira' %}** uma coleçao de discos de música brasileira, do
regional até algumas coisas experimentais. Ele gravou discos do Elomar,
Quinteto Armorial, Guerra Peixe, etc. No torrent tem quase toda a
coleção dele.

**{% el 'magnet:?xt=urn:btih:854fda7fdcbfb37be7c6c94b1590a7fb3e62839f&dn=Hegel%20e%20coment%C3%A1rios&tr=udp%3A%2F%2Fzephir.monocul.us%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker1.wasabii.com.tw%3A6969%2Fannounce' 'Hegel e comentários' %}** conjunto de textos do Hegel e comentadores
relevantes.
