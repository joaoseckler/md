keywords: joão,seckler,música,music,lyrics,sheet,music,lilypond
shortdescription: Lyrics to lilypond
hascomments: 1
lang: en
footerlink: stuff

{% load lyrics2ly %}

## LilyPond scansion

This software helps writing lyrics in [`lilypond`](https://lilypond.org).
Given some text and a language it will:

 1. Break all words into syllables, separating them with double hyphens;
 2. Escape existing single hyphens with a backslash.


<br>

{% lyrics2ly_form %}

<br>

This is useful for aligning lyrics with music in a `\lyricmode` block,
but is not sufficient. Melismas and elisions should be handled manually.
For more information on aligning lyrics and music with `lilypond`, see
{% el 'https://lilypond.org/doc/v2.23/Documentation/learning/aligning-lyrics-to-a-melody' 'this documentation page' %}.

This page relies on a python script which you can also download
{% el 'https://gitlab.com/joaoseckler/lyrics2ly' 'here' %}
(in order to, for example, integrate the script with your text editor).

### Tips for `vim` users

If you download this script you could, naturally, define some shortcut
(say, `<leader>-l`) to apply the script to a visual selection:

```vim
autocmd FileType lilypond vnoremap <Leader>l :!python /path/to/lyrics2ly.py<Enter>
```

Other lilypond configuration snippet I find very useful for writing
lyrics is binding `ctrl-space` to insert double hyphens:

```vim
autocmd FileType lilypond inoremap <C-@> <Space>--<Space>
autocmd FileType lilypond nnoremap <C-@> a<Space>--<Space><Esc>l
```
