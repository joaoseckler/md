venv="set -a; . .env; set +a; . venv/bin/activate"

cmd=(
  "cd jseckler.xyz/md &&"
  " git pull origin master && cd ../ &&"
  " $venv &&"
  " ./makepages.py &&"
  " ./manage.py collectstatic --no-input"
)

ssh jseckler.xyz "${cmd[*]}"
