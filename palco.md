keywords: joão,seckler,booklet,pdfjam,pdf,convert,signature
shortdescription: Moral da história: continue se reposicionando mesmo
                  depois de você já achar que está no seu lugar.
hascomments: 1
lang: pt
footerlink: stuff
css: palco/palco.css
js_head: palco/bundle.js
         defer

{% load palco %}

## Guia de movimentação de palco para coralistas

Quando um coro vai cantar, ele precisa se colocar no palco de algum
jeito. Pode até parecer simples, mas nem sempre dá um resultado
legal. Quando o objetivo é o coro organizar-se num arco, por exemplo,
é comum observar um formato de "baleia":

<svg xmlns="http://www.w3.org/2000/svg" width="475.386" height="134.681" viewBox="-10 -10 145.779 55.634"><path d="M45.574 120.595c.291-78.923 125.577-1.613 125.577-1.613" style="fill:none;stroke:#000;stroke-width:.264583px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" transform="translate(-45.442 -84.961)"/></svg>

Nos parece que compete a cada coralista executar os seguintes passos,
que são simples mas não necessariamente óbvios:

1. Imaginar a forma final do coro e colocar-se nela;
2. Colocar-se no ponto equidistante a todos os colegas mais próximos;
3. Não parar de repetir os passos 1 e 2 até começar a cantar.

Para ver essas regras ação, vamos pôr umas bolinhas para segui-las:

{% palco %}

Ó lá! Bastou seguir as regras que elas formaram um arco nada embaleiado.
E repare como elas não são tão rápidas: demora um pouco para todas se
alinharem. O que acaba acontecendo, muitas vezes, é que os coralistas
desistem de se preocupar com sua posição muito cedo. Vamos ver como
algumas bolinhas menos insistentes se saem:

{% palco lazy=True %}

Moral da história: continue se reposicionando mesmo depois de você já
achar que está no seu lugar.

<hr>

Essas regras também funcionam para outros formatos. Se o seu coro é um
pouco grande, provavelmente você já esteve numa formação mais ou menos
assim:

{% palco n_singers=30 formation="double_arc" %}

Ou, se você canta no
{% el 'https://www.youtube.com/watch?v=MvJFC8Zicfw' 'graindelavoix' %},
elas também funcionam.

{% palco n_singers=10 formation="circle" %}

Até mesmo para formações menos ordenadas a coisa funciona! Por exemplo,
se o coro quiser organizar-se de maneira "aleatória" pelo espaço.

{% palco n_singers=30 formation="rectangle" %}

Nesse caso, as bolinhas ficam meio malucas porque nunca conseguem de
fato se encaixar num ponto equidistante em relação aos coralistas mais
próximos: tudo bem! Fazer como elas — ficar procurando esse ponto
até começar a cantar — deve resultar numa formação final razoável.

<hr>

Se quiser saber mais ou brincar com as animacoes dessa página,
clique {% il 'palco/animacoes' 'aqui' %}.
