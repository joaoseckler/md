shortdescription: Me!
title: João Seckler
lang: en
pt_version: eu
nocomments: 1

## Me

<br>

{% el 'https://gitlab.com/joaoseckler' 'gitlab' %}

{% el 'https://github.com/joaoseckler' 'github' %}

{% el 'https://www.cifraclub.com.br/musico/18251978/' 'cifraclub' %}

{% il 'teaching' 'teaching' %}

jseckler at riseup.net
