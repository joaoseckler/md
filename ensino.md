keywords: joão,seckler,ensino,css,javascript,python,arco
shortdescription: Materiais de ensino
lang: pt
en_version: teaching
footerlink: index

## Ensino

**{% el 'https://arco.coop.br/~jseckler' 'Página pessoal no site da Arco' %}**

Materiais dos cursos de matemática e computação que dei na Arco Escola Cooperativa.

**{% il 'ensino/palavras' 'Formação na Palavras' %}**

Material de um curso sobre CSS e Javascript oferecido para a equipe da
{% el 'https://palavraseducacao.com.br' 'Palavras Projetos editoriais' %}.
