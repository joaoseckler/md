keywords: joão,seckler,música,elomar,funcao,campo,branco
shortdescription: Algumas partituras
hascomments: 1
lang: pt
footerlink: coisas
en_version: sheet-music

## Partituras

**Sambas**

- *{% lf 'musica/encontro-sambado.pdf' "Encontro sambado" %}*, que meu
  amigo R. fez e eu musiquei
- *{% lf 'musica/efeito-borboleta.pdf' "Efeito borboleta" %}*, outra do
  R. que eu musiquei

**{% lf 'musica/modinha.pdf' "Modinha" %}**, de Tom Jobim e Vinícius de
  Moraes, num arranjo meu para coro.

**{% lf 'musica/ta-chovendo.pdf' "Tá chovendo" %}**, um choro que fiz em
2024

**{% lf 'musica/sabonete.pdf' "Sabonete" %}**, outro choro que fiz entre
2024 e 2025

**Outras** Essa partituras foram criadas com
{% el 'https://lilypond.org' 'lilypond' %} e os códigos fonte estão
disponíveis {% el 'https://gitlab.com/joaoseckler/partituras' 'aqui' %}.

{% partituras as parts %}
<ul>
{% for p in parts %}
<li>
    {% sl p.url p.title %} <em>{{ p.composer }}</em>
</li>
{% endfor %}
</ul>


**Transcrição de canções do Elomar** A princípio, são canções do
álbum "Na quadrada das águas perdidas". Ele você encontra no
youtube ou no torrent do Marcus Pereira (veja {% il 'torrents' 'aqui' %}).
O nível de precisão das transcrições não está muito consistente;
umas horas elas pegam mais detalhes, outras horas menos. Os versos,
a não ser por erro de transcrição, são bem confiáveis: copiei direto
do encarte do LP. Além da letra, transcrevi algumas anotações,
também do encarte, inclusive um pequeno glossário para cada música.
No geral, na música do Elomar o aspecto violonístico é bem
importante, e, infelizmente, isso fica aqui um pouco de lado, em
favor de uma coisa mais genérica. Para fazer as partituras, usamos
{% el 'https://lilypond.org' 'lilypond' %}. O código fonte delas está
disponível {% el 'https://gitlab.com/joaoseckler/transcribing-elomar' 'aqui' %}.
Se tiver sugestões, correções, etc, por favor me escreva, deixe um
{% il 'partituras/discussão' 'comentário' %} ou mande direto um MR.

- {% il 'musica/elomar/a-meu-deus-um-canto-novo' 'A meu Deus um canto novo' %} <br>
- {% il 'musica/elomar/a-pergunta' 'A pergunta' %} <br>
- {% il 'musica/elomar/campo-branco' 'Campo Branco' %} <br>
- {% il 'musica/elomar/funcao' 'Função' %} <br>
- {% il 'musica/elomar/na-quadrada-das-aguas-perdidas' 'Na quadrada das águas perdidas' %} <br>
