keywords: joão,seckler,música,elomar,funcao,campo,branco
shortdescription: Some scores
hascomments: 1
lang: en
footerlink: stuff
en_version: partituras

## Sheet music

**Sambas**

- *{% lf 'musica/encontro-sambado.pdf' "Encontro sambado" %}*, which my
friend R. composed and I settled the music around
- *{% lf 'musica/efeito-borboleta.pdf' "Efeito borboleta" %}*, another
  one from R. and me

**{% lf 'musica/ta-chovendo.pdf' "Tá chovendo" %}**, a choro I composed
in 2024.

**{% lf 'musica/sabonete.pdf' "Sabonete" %}**, another choro I made
between 2024 and 2025

**{% lf 'musica/modinha.pdf' "Modinha" %}**, by Tom Jobim and Vinícius de
  Moraes, in a choral arrangement made by me.

**Other** These scores were created with
{% el 'https://lilypond.org' 'lilypond' %} and their source codes
are available
{% el 'https://gitlab.com/joaoseckler/partituras' 'here' %}.

{% partituras as parts %}
<ul>
{% for p in parts %}
<li>
    {% sl p.url p.title %} <em>{{ p.composer }}</em>
</li>
{% endfor %}
</ul>

**Transcribing Elomar** At least for now, songs from the album "Na
quadrada das águas perdidas". Find it at youtube or in the
{% il 'torrents_en' 'torrent' %} with Marcus Pereira's records . The
transcriptions' level of detail is not very consistent. The lyrics,
except for any transcription errors, are rather reliable: they were
copied from the LP's liner notes. From these liner notes I also
transcribed some annotions, including a small glossary for each
song. The music of Elomar is quite centered around the acoustic
guitar and, unfortunately, this aspect is left a bit aside.The
software {% el 'https://lilypond.org' 'lilypond' %} was used for the
transcriptions. The source code is available
{% el 'https://gitlab.com/joaoseckler/transcribing-elomar' 'here' %} If you
have suggestions, corrections, etc, please write me, leave a
{% il 'sheet-music/comments' 'comment' %} or send a MR.

* {% il 'musica/elomar/a-meu-deus-um-canto-novo' 'A meu Deus um canto novo' %} <br>
* {% il 'musica/elomar/a-pergunta' 'A pergunta' %} <br>
* {% il 'musica/elomar/campo-branco' 'Campo Branco' %} <br>
* {% il 'musica/elomar/funcao' 'Função' %} <br>
* {% il 'musica/elomar/na-quadrada-das-aguas-perdidas' 'Na quadrada das águas perdidas' %} <br>
