keywords: joão,seckler,html,greencloth,riseup,we
shortdescription: Converta HTML em GreenCloth (Riseup We)
hascomments: 1
lang: pt
footerlink: stuff
css: html2gc/style.css
js_head: https://unpkg.com/turndown/dist/turndown.js
js_body: html2gc/script.js


## Converta textos formatados para GreenCloth

GreenCloth é a versão de Markdown utilizada pela
[Riseup We](https://we.riseup.net). Para converter, copie o texto
formatado e cole com Ctrl-V nessa página.

<div class="codehilite"><pre><code class="html2gc" spellcheck="false" contenteditable></pre></pre></div>


Nem todas as opções estão implementadas. Para mais informações sobre
greencloth, veja [aqui](https://we.riseup.net/do/static/greencloth).
Para melhorar esse programa, [aqui](https://gitlab.com/joaoseckler/website/-/tree/master/html2gc).
