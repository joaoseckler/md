keywords: joão,seckler,elomar,função,transcrição,música,letra,relampo
shortdescription: Partitura, letra e comentários sobre músicas do Elomar
lang: pt
footerlink: partituras

## Função

Transcrição de canções do Elomar
{ .subtitle }

Clique {% sl 'musica/elomar/funcao.pdf' 'aqui' %} para baixar o pdf.

<p class="center-aligned">
<a href="{% static 'musica/elomar/funcao-0.jpg' %}">
  <img src="{% static 'musica/elomar/funcao-0.jpg' %}" width="90%"></a>
<a href="{% static 'musica/elomar/funcao-1.jpg' %}">
  <img src="{% static 'musica/elomar/funcao-1.jpg' %}" width="90%"></a>
</p>

### Letra

```text
Vem João
trais as viola siguro na mão
pega a manduréba atiça os tição
carrega por terrêro os banco e as cadêra
e chama as minina prá rodá o baião
nós dois sentado junto da foguera
vamo fazê a nossa brincadêra e cantar
a lijêra, moda de lavação
em homenage ao nosso São João
e prá acabá com a saudade matadêra
você canta lijêra, canto moirão
você canta lijêra, canto moirão
ai meu São João, lá da aligria
ai meu São João, lá da aligria
a saudade cada dia mais me doi no coração
vem João
vamos meu bichin cantá o moirão
tem um bicho roeno o meu coração
cuano eu era minino a vida era manêra
não pensava na vida junto da foguêra
brincando com os irmão a noite intêra
sem me dá qui êsse tempo bom havera de passé
e a saudade me chegá essa fera
quem pensar que êsse bicho é da cidade
s'ingana a saudade nasceu cá no sertão
na bêra da foguêra de São João
ai meu São João, lá da aligria
ai meu São João, lá da aligria
a saudade cada dia mais me doi no coração
```

### Anotações

```text
FUNÇÃO foi composta em 1968; no dia em que nasceu
João Ernesto, o segundo filho de Elomar, reflete
uma evocação à beira da fogueira. No nordeste,
principalmente no interior, como é o caso da
Bahia, o São João e o ciclo junino, de um modo
geral, são cultivados ainda com a pureza e
tradição transmitidas pela Península Ibérica.

A alegria, a nostalgia, e mesmo uma profunda
saudade dos tempos de infância são aqui
plenamente desenvolvidos.

"Mandureba"  - cachaça ou pinga de 2ª qualidade
"lijêra"     - gênero de cantoria antiga, já em
desuso no nordeste.
"Moirão"     - gênero de cantoria em desafio; a
expressão é muito comum na obra de Elomar; pode
ser composto de 5, 6 e até mesmo 7 versos.
"rueno"      - roendo
"cuano"      - corruptela de quando.
"havera"     - tradicionalmente usado pelos
catingueiros como: haveria.
```
