keywords: joão,seckler,elomar,na,quadrada,das,aguas,perdidas,transcrição,música,letra
shortdescription: Partitura, letra e comentários sobre músicas do Elomar
lang: pt
footerlink: partituras


## Na quadrada das águas perdidas

Transcrição de canções do Elomar
{ .subtitle }

Clique {% sl 'musica/elomar/na-quadrada-das-aguas-perdidas.pdf' 'aqui' %} para baixar o pdf.

<p class="center-aligned">
<a href="{% static 'musica/elomar/na-quadrada-das-aguas-perdidas-0.jpg' %}">
  <img src="{% static 'musica/elomar/na-quadrada-das-aguas-perdidas-0.jpg' %}" width="90%"></a>
<a href="{% static 'musica/elomar/na-quadrada-das-aguas-perdidas-1.jpg' %}">
  <img src="{% static 'musica/elomar/na-quadrada-das-aguas-perdidas-1.jpg' %}" width="90%"></a>
</p>

### Letra

```text
Da carantonha mili légua a caminhá
muito mais, inda mais, muito mais
da Vaca Sêca, Sete Varge inda pr'á lá
muito mais, inda mais, muito mais
Dispois dos derradêro cantão do sertão
lá na quadrada das águas perdida
Rêis, Mãe-senhora
beleza isquicida
bens, a lagoa arriscosa função

Ôh Câindo chiquera as cabra mais cedo
aparta os cabrito mi cura Segredo
chincha Lubião, esse bode malvado, travanca o chiquêro
ti avia a cuidar

Alas qui as polda di Sheda rincharo ao luá
na madrugada suadas de medo pr'á lá
Runcas levando acesas candeia inlusão

Da carantonha mili légua a caminhá
mil badaronha tem que tê pr'á chegá lá
Sete jinela sete sala um casarão

Laço dos Moura
Varge dos trumento
Velhos domingos
Casa dos Sarmentos
Moças, sinhoras
mitriosa função
Dá pressa in Guilora a ingomá nossos terno
Albarda as jumenta cum as capa de inverno
cuida as ferramenta num dêxa ela vê
Si não pode ela num anui nois i
Onte pr'os norte de Mina o relampo raiô
Mucadim a mão do ri as água já tomô
Anda muntemo mondengo pr'á nois i prá lá
```

### Anotações

```text
A Quadrada das águas perdidas que dá nome ao Disco é
uma obra extremamente curiosa, pois é marcada pelo
intemporal e pelo infinito do espaço. Ela narra uma
misteriosa funão que vai ocorrer longe, tão longe
onde torna-se impossível ir a cavalo, pois fica a
1.000 léguas ainda p'rá lá, e reunindo Reis, mães e
senhoras de tempos redivivos.

A imagem é portanto ficcional, pois as distâncias e
pessoas são inconciliáveis, caindo naquilo que Elomar
chama de "a dobra do tempo".

A Quadrada das águas perdidas é trabalho recente, do
ano de 1978 e observe-se o caminhar ligeiro da
estrutura melódica estabelecendo claramente a noção
de distância e do caminhar dos animais naquela
direção.


"arriscosa função" - uma festa arriscada, com perigos
próprios até mesmo por ser em lugar de difícil acesso

"alas qui" - Expressão caatingueira: eis que,
acontece que, consta que

"as poldas de Sheda" - poldos, águas ainda jovens;
Sheda, nome próprio, proprietário das poldas.

"runcas" - a expressão é em homenagem a uma "dona
Runauinha", figura lendária na Caatinga, como senhora
alegra, festeira; daí a expressão se sedimentou.
Senhoras festeiras, comuns no sertão.

"badaronha" - expressão bastante comum na Caatinga
Baiana, tem o sentido de expediente, maneirice,
artifícios. Ex: Fulano é cheio de Badaronha.

"mitriosa Função" - misteriosa função.

"albarda" - arreia os jumentos. (do verbo albardar)

"ferramentas" - armas: fação, pistola, etc.

"anui" - do verbo anuir, consentir

"Guilora" - nome próprio - Glória

"mucadim" - pode acontecer que

"a mãe do ri" - a mãe do Rio, ou seja, o primeiro
leito do rio, o leito mais fundo

"muntemo o mondengo" - expressão simbólica, regional,
poderia ser entendida como: tomemos iniciativa;
lutemos com força. É curioso que mondêgo é um rio em
Portugal, difícil de atravessar à época das chuvas;
como a expressão se fundiu no vocabulário do
sertanjeto é que dá pr'á pensar... Essa expressão foi
ouvida pelo Elomar na chapada Diamantina, de um velho
de 110 anos.

"Chincha" - do verbo chinchar
```
