keywords: joão,seckler,elomar,função,transcrição,música,letra,relampo
shortdescription: Partitura, letra e comentários sobre músicas do Elomar
lang: pt
footerlink: partituras

## A meu Deus um canto novo

Transcrição de canções do Elomar
{ .subtitle }

Clique {% sl 'musica/elomar/a-meu-deus-um-canto-novo.pdf' 'aqui' %} para baixar o pdf.

<p class="center-aligned">
<a href="{% static 'musica/elomar/a-meu-deus-um-canto-novo-0.jpg' %}">
  <img src="{% static 'musica/elomar/a-meu-deus-um-canto-novo-0.jpg' %}" width="90%"></a>
<a href="{% static 'musica/elomar/a-meu-deus-um-canto-novo-1.jpg' %}">
  <img src="{% static 'musica/elomar/a-meu-deus-um-canto-novo-1.jpg' %}" width="90%"></a>
</p>

### Letra

```text
Bem de longe na gradne viagem
Sobrecarregado paro a descansar,
emergi de paragens cigans
pelas mãos de Elmana, santas como a luz
e em silêncio contemplo, então
mais nada a revelar
fadigado e farto de clamar às pedras
de ensinar justiça ao mundo pecador
oh lua nova quem me dera
eu me encontrar com ela
no pispei de tudo
na quadra perdida
na manhã da estrada
e começar tudo de nôvo

topei in certa altura da jornada
com um qui nem tinha pernas para andar
comoveu-me em grande compaixão
voltano o olhar para os céus
recomendou-me ao Deus
Senhor de todos nós rogando
nada me faltar
resfriando o amor a fé e a caridade
vejo o semelhante entrar em confusão
oh lua nova quem me dera
eu me encontrar com ela
no pispei de tudo
na quadra perdida
na manhã da estrada
e começar tudo de nôvo

bôas novas de pena alegria
passaram dois dias da ressureeição
refulgida uma beleza estranha
que emergiu da entranha
das plagas azuis
num esplendor de gloria
avistaram u'a grande luz
fadigado e farto de clamar às pedras
de propor justiça ao mundo pecador
vô prossiguino istrada a fora
ruma à istrêla canora
e ao Senhor das Searas a Jesus eu lôvo
levam os quatro ventos
ao meu Deus um canto nôvo
```

### Anotações

```text
A meu Deus um canto novo surgiu na quadra das águas
em 1977.  Muitos, através dos tempos, ofertaram a
Deus, e temos como exemplo Bach, Mendhelson, Haidyn e
outros. Porque, segundo o poeta, não fazer cânticos a
Deus, hoje? O Rei Davidem sua poética pediu: Louvai a
Deus sempre com um cnato novo. A canção vem duma
viagem nas Marrecas, cabeceiras do Rio Gavião, em
agosto de 1977.

Numa curva da estrada antes de atravessar o leito do
rio Gavião, o cantor daquele lugar estanca o carro
pelo meio dia, o sol mordendo e o chão escaldante,
diante da figurad estranha de um homem aleijado e de
espirito afeito às dimensões superiores; com o rosto
enxarcado de suor e o olhar mergulhado no azul do
último céu, fala de conceitos e coisas que muito
raramente é dado aos mortais ouvir.  Sorrindo feliz
agradece a Deus louvando-o por sentir-se grandemente
honrado pela atenção dispensada por um semelehante,
pois nunca alguém havia posado diane dêle. Esse tipo
de andarilho e de errante é comum na Caatinga.

"a grande viagem" - o sentido é da tradição errante
cigana. Os

"filhos do vento" perigrinando na terra, vêm próximo
o dia do parar definitivo, ponto final de um processo
enunciado e cumprido há sećulos.

"emergi de paragens ciganas" - também vinculado à
tradição cigana, conta a tradição que os ciganos
vivem felizes, mas um dia (repetição da visão
edênica?) foram expulsos e condenados a perigrinar na
terra. Emergir, no sentido de vir ao vosso mundo.

"topei" - encontrei

"no pispei de tudo" - no início de tudo

"istrêla canora" - Estrela que canta
```
