keywords: joão,seckler,elomar,campo,branco,transcrição,música,letra,relampo
shortdescription: Partitura, letra e comentários sobre músicas do Elomar
lang: pt
footerlink: partituras

## Campo Branco

Transcrição de canções do Elomar
{ .subtitle }

Clique {% sl 'musica/elomar/campo-branco.pdf' 'aqui' %} para baixar o pdf.

<p class="center-aligned">
<a href="{% static 'musica/elomar/campo-branco-0.jpg' %}">
  <img src="{% static 'musica/elomar/campo-branco-0.jpg' %}" width="90%"></a>
<a href="{% static 'musica/elomar/campo-branco-1.jpg' %}">
  <img src="{% static 'musica/elomar/campo-branco-1.jpg' %}" width="90%"></a>
</p>


### Letra

```text
Campo branco minhas penas que pena secou
todo o bem qui nois tinha era chuva era o amor
num tem nada não nois dois vai penano assim
campo lindo ai qui tempo ruim
tu sem chuva e a tristeza em mim
peço a Deus a meu Deus grande Deus de Abraão
prá arrancar as penas do meu coração
dessa terra sêca in ança e aflição
todo bem é de Deus qui vem
quem tem bem lôva a Deus seu bem
quem não tem pede a Deus qui vem
pela sombra do vale do ri Gavião
os rebanhos esperam a trovoada chover
num tem nada não também no meu coração
vô ter relampo e trovão
minh'alma vai florescer
quando a amada e esperada trovoada chegá
iantes da quadra as marrã vão tê
sei qui inda vô vê marrã pari sem querer
amanhã no amanhecer
tardã mais sei qui vô ter
meu dia inda vai nascer
e esse tempo da vinda tá perto de vin
sete casca aruêra cantaram prá mim
tatarena vai rodá vai botá fulô
marela du u'a veis só
prá ela de u'a veis só.
```


### Anotações

```text
Campo Branco, expressão que corresponde à
Caatinga, de origem indígena. Esta música foi
composta em 1974 e, pela estrutura da letra e da
música ela é na realidade o cântico da vinda da
chuva. Definido como uma música onde os versos
brotam de dentro dos versos, ela identifica uma
noção de felicidade, de "totalidade" do
Caatingueiro: o amor e a chuva.

Dessa relação de bem-venturança o Homem, a
Natureza e os bichos se integram em um único
processo: o da sobrevivência.


"Campo Branco" - tradução de Caatinga, essa
expressão é indígena

"Dessa terra seca in ança" - dessa terra seca, em
ânsia, com o sentido do adjetivo de ansiedade.

"Vô ter relampo e trovão" - assim como a terra
ansia por relâmpagos e trovões, enunciadores da
chuva, meu coração também o faz, pela fartura que
estes propiciam.

"iantes da quadra as marrã vão ter" - Antes mesmo
do ciclo biológico das cabras, elas vão parir.
Marrã no sentido cabra nova, parideira.

"e esse tempo da vinda tá perto de vim" - a
expressão é bíblica e se estrutura numa profecia:
os tempos da ressurreição estão próximos. Essa
abordagem é uma constante no trabalho de Elomar.

"sete casca aruêra" - árvore medicinal, comum na
zona caatingueira.

"tatarena vai rodá vai botá fulô" - árvore que
abre em flor, amarela como enunciadora da chiva

"marela" - corruptela de amarelo
```

Uma observação sobre essas anotações do encarte. Na entrada "Vô ter
relampo e trovão", me parece que o coração do eu lírico não está
ansiando "pela fartura" que o campo com chuva propicia, mas sim pela
volta do amor dele. Veja, por exemplo, os versos "todo o bem qui nois
tinha era chuva era o amor" e "prá ela de u'a veis só". Parecem ser
duas histórias em paralelo, uma se confundindo com a outra; a do campo
que anseia a chuva e a do sertanejo que anseia por seu amor.
