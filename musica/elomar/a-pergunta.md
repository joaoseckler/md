keywords: joão,seckler,elomar,função,transcrição,música,letra,relampo
shortdescription: Partitura, letra e comentários sobre músicas do Elomar
lang: pt
footerlink: partituras

## A pergunta

(do "O tropeiro Gonsalin")
{ .subtitle }

Clique {% sl 'musica/elomar/a-pergunta.pdf' 'aqui' %} para baixar o pdf.

<p class="center-aligned">
<a href="{% static 'musica/elomar/a-pergunta-0.jpg' %}">
  <img src="{% static 'musica/elomar/a-pergunta-0.jpg' %}" width="90%"></a>
<a href="{% static 'musica/elomar/a-pergunta-1.jpg' %}">
  <img src="{% static 'musica/elomar/a-pergunta-1.jpg' %}" width="90%"></a>
</p>

### Letra

```text
Ô Quilimero assunta meu irirmão
iantes mêrmo quenóis dois saudemo
eu te pregunto naquele refrão
qui na fartura nóis sempre cantemo
na catinga tá chuveno
ribeirão istão incheno
na catinga tá chuveno
ribeirão istão incheno
me arresonda meu irirmão
cuma o povo de lá tão
só a terra que você dexô
quinda tá lá não ritirou-se não
os povo as gente os bicho as coisa tudo
uns ritirou-se in pirigrinação
os ôtro os mais velho mais cabiçudo
voltaro pru qui era pru pó do chão
adispois de cumê tudo
cumêr' precata surrão
cumêr' côro de rabudo
cumêr' cururú rodão
e as cacimba do ri gavião
já deu mais de duas cova d'um cristão
inté aquela a da cara fêa
se veno só dexô a terra alêa
foi nas pidrinha cova de serêa
vê sua madrinha
e vêi de mão c'ua vea
a dispois de cumer tudo
for rir comero arêa
comeram coro de rabudo
capa de cangaia veia
na catinga morreu tudo
qui nem preciso caxão
meu cumprade João Barbudo
num cumpriu obrigação
vai prá mais de duas lua
que meu pai mandô eu i no Nazaré
buscá u'a quarta de farinha
eu e o irmão Zé Bento vinha andano a pé
mãe lua magrinha qui está no céu
será que cuano eu chegue in minha terra
aina vô incontrá o qui é meu
será que Deus do céu aqui na terra
de nosso povo intonce se isqueceu
na catinga morreu tudo
qui nem preciso caxão
meu cumprade João Barbudo
num cumpriu obrigação
udo ão udo ão.
```

### Anotações

```text
FUNÇÃO foi composta em 1968; no dia em que nasceu
João Ernesto, o segundo filho de Elomar, reflete
uma evocação à beira da fogueira. No nordeste,
principalmente no interior, como é o caso da
Bahia, o São João e o ciclo junino, de um modo
geral, são cultivados ainda com a pureza e
tradição transmitidas pela Península Ibérica.

A alegria, a nostalgia, e mesmo uma profunda
saudade dos tempos de infância são aqui
plenamente desenvolvidos.

"Mandureba"  - cachaça ou pinga de 2ª qualidade
"lijêra"     - gênero de cantoria antiga, já em
desuso no nordeste.
"Moirão"     - gênero de cantoria em desafio; a
expressão é muito comum na obra de Elomar; pode
ser composto de 5, 6 e até mesmo 7 versos.
"rueno"      - roendo
"cuano"      - corruptela de quando.
"havera"     - tradicionalmente usado pelos
catingueiros como: haveria.
```
