keywords: joão,seckler,booklet,pdfjam,pdf,convert,signature,livreto
shortdescription: Converta PDFs para formato de livreto
hascomments: 1
lang: pt
footerlink: coisas
en_version: booklet

{% load booklet %}

## Conversão para formato de livretos

Forneça um arquivo PDF e devolveremos ele em formato de livreto.

{% booklet_form %}


### O que é livreto?

Um PDF gerado aqui vai ter duas páginas do arquivo original em cada uma
de suas próprias páginas. Ele é organizado de forma que, depois de
impresso, basta seguir os seguintes passos para arranjar suas páginas na
ordem correta:

1. Dividir o documento em blocos, cada um contendo _n_ folhas. Cada
   bloco é chamado de **livreto**;
2. Dobrar cada livreto no meio;
3. Juntar os livretos dobrados, mantendo a ordem de impressão.

Aqui, o valor de _n_ depende do tamanho de livreto escolhido. Esse
tamanho determina o número de páginas do arquivo original que estarão
presentes em cada livreto. Visto que cada folha contém quatro dessas
páginas, _n_ é igual ao tamanho do livreto dividido por quatro.


### Por que essa página?

Impressão em formato de livreto é bastante comum em vários softwares de
impressão, mas normalmente só é possível imprimir um livreto por vez.
Essa página é útil se você tiver um PDF grande que quiser transformar em
vário livretos, e imprimir tudo de uma vez.
